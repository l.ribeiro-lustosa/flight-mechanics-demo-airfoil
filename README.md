# Flight Mechanics Airfoil Demo in C/C++

This repo contains an Airfoil demo app I use to lecture about airfoil aerodynamics, solving differential equations through state-space dynamic functions and Runge-Kutta in C/C++, modeling, and others. Feel free to use it as you will! I have tested it only on Ubuntu; other operating systems are not yet tested. Feel free to contribute too! =)

## What is interesting about this demo?

This demo is a good example on:

 - Airfoil global aerodynamics (try hitting unusual angle of attacks! The app won't break, and will output something that seems physically reasonable). The motivation for this are tail-sitting flying-wings, such as the **MAV**ION (see our lab [website](http://www.ionlab.fr) for more info). For more info on global aerodynamics for tail-sitters, please check our AIAA JGCD paper [here](https://arc.aiaa.org/doi/abs/10.2514/1.G003374).
 - How to compute Runge-Kutta using C/C++ libraries;
 - How to plot in real-time animations from dynamic systems using the Game Engine [raylib](https://www.raylib.com/);
 - How to model mechanics systems with constraints (using generalized inertias and forces).

## Requirements

 - eigen (included as a git submodule);
 - odeint-v2 (included as a git submodule);
 - raylib (not included, please install it according to [this](https://www.raylib.com/) before compilation).

## How to compile it

Start by cloning this repo with all its submodules:

    git clone --recurse-submodules https://gitlab.isae-supaero.fr/l.ribeiro-lustosa/flight-mechanics-demo-airfoil.git

Then run **make** in its root directory. This generates a **game** binary that you can run with:

    ./game

If you installed raylib in a nonstardard system location, you might have to change the Makefile accordingly.

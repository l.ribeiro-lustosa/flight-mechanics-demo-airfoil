#include "raylib.h"
#include "raymath.h"
#include <iostream>
#include "Eigen/Dense"
#include <cmath>

#include <boost/array.hpp>
#include <boost/numeric/odeint.hpp>

// window size [pixels]
#define WINDOW_WIDTH 1280
#define WINDOW_HEIGHT 720
// vertical distance between two parameter containers [pixels]
#define VERTICAL_DISPLAY_OFFSETS 60
// define pixel to meter world equivalence (value chosen so airfoil figure has chord 1m)
// note: airfoil drawing is 210 pixels wide approximately
#define PX2M 1.0 / 210.0
#define M2PX 210.0 / 1.0
// defines maximum speed allowed in the app for user input
#define WIND_SPEED_MAX 10.0
// image in the app is WIND_TEXTURE_SCALE pixels larger than original one in PNG file
#define WIND_TEXTURE_SCALE 3

/* Aerodynamic coefficients */
// force coefficients (drag, sideforce, lift) described in wind axes
double Cd(double alpha);
double Cy(double alpha);
double Cl(double alpha);
// moment coefficients at the aerodynamic center (quarter-chord) described in body axes
double Kl(double alpha);
double Km(double alpha);
double Kn(double alpha);

/* this function displays a container of user input data (e.g., wind incidence, wind speed, CG location) */
void data_display_for_inputs(int posX, int posY, const char* display_text, double display_value, double bar_percentage);
/* helper functions and structures for integrating differential equations of motion */
// state size definition: x = (pos,vel,pitch,pitch_rate), where:
// - pos is in R(3) and defines the position of the upper-left-corner of airfoil image in window coordinates [m]
// - vel is in R(3) and is the derivative of the above position, with no change of coordinates [m/s]
// - pitch is in R(1) and it is the angle between airfoil reference frame and window [rad]
// - pitch rate is in R(1) and it is the derivative of the above quantity [rad/s]
// PS: I made the state definition tricky on purpose, to test myself with transport theorems of mechanics for fun
typedef boost::array< double , 8 > state_type;
// x_dot = f(x,t) dynamics function of the pinned airfoil according to the above state definition
void airfoil_dynamics( const state_type &x , state_type &dxdt , double t );
// this connects the RK solver to our global variables in this problem
void update_airfoil( const state_type &x , const double t );

double rho{1.295};
double Sref{1};
double chord{1};
double wingspan{1};

double wind_speed{0.0};
double wind_angle{145.0*DEG2RAD};

/* airfoil geometric and aerodynamic properties */
Eigen::Vector3d pos_CG_TX_B(140.0*PX2M, 0.0, 17.0*PX2M);
Eigen::Vector3d pos_AC_TX_B(150.0*PX2M, 0.0, 17.0*PX2M);
Eigen::Vector3d pos_PV_TX_B(175.0*PX2M, 0.0, 17.0*PX2M);
Eigen::Vector3d pos_AC_CG_B;
Eigen::Vector3d pos_PV_CG_B;

// airfoil mass in [kg]
double mass = 0.1;
// inertia tensor of airfoil
Eigen::Matrix3d Jb;

Eigen::Vector3d airfoil_Pos_Tx_I_I;
Eigen::Vector3d airfoil_Vel_Tx_I_I;
double pitch;
double pitch_rate;

/* wind input */
Eigen::Vector3d wind_Vel_I(0, 0, 0);
Eigen::Vector3d wind_Pos_I(0, 0, 0);

int main()
{

    InitWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Flight Mechanics [Airfoil Demo]");

    Texture2D cloudsTex     = LoadTexture("textures/cloudss.png");
    Texture2D airfoilTex    = LoadTexture("textures/airfoil.png");
    Texture2D cgTex         = LoadTexture("textures/cg.png");
    Texture2D pivotTex      = LoadTexture("textures/pivot.png");
    Texture2D backgroundTex = LoadTexture("textures/background.png");

    Jb << 0.1, 0.0, 0.0,
          0.0, 0.1, 0.0,
          0.0, 0.0, 0.1;

    /* airfoil state definition */
    // airfoil position wrt window origin in window reference frame [m]
    airfoil_Pos_Tx_I_I << 1.0, 0.0, 1.0;
    // airfoil velocity wrt window in window reference frame [m/s]
    airfoil_Vel_Tx_I_I << 0.0, 0.0, 0.0;
    // airfoil initial pitch angle [rad]
    pitch = 30.0 * DEG2RAD;
    // airfoil initial pitch rate [rad/s]
    pitch_rate = 0.0 * DEG2RAD;

    SetTargetFPS(60);

    // mouse user input state machine variables
    bool ClickedAngleOfAttack{false}, ClickedWindSpeed{false}, ClickedCG{false}, ClickedPivot{false};

    while (!WindowShouldClose())
    {

        BeginDrawing();
        ClearBackground(WHITE);

        /* catch user mouse input */
        int mouseX, mouseY;

        // this function catches the first click
        if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT)) {
            mouseY = GetMouseY();
            if ( mouseY >= 80 + 0*VERTICAL_DISPLAY_OFFSETS && mouseY <= 80 + 1*VERTICAL_DISPLAY_OFFSETS ) 
                ClickedAngleOfAttack = true;
            if ( mouseY >= 80 + 1*VERTICAL_DISPLAY_OFFSETS && mouseY <= 80 + 2*VERTICAL_DISPLAY_OFFSETS ) 
                ClickedWindSpeed = true;
            if ( mouseY >= 80 + 2*VERTICAL_DISPLAY_OFFSETS && mouseY <= 80 + 3*VERTICAL_DISPLAY_OFFSETS ) 
                ClickedCG = true;
            if ( mouseY >= 80 + 3*VERTICAL_DISPLAY_OFFSETS && mouseY <= 80 + 4*VERTICAL_DISPLAY_OFFSETS ) 
                ClickedPivot = true;
        // this function catches if user is holding pressed
        } else if (IsMouseButtonDown(MOUSE_BUTTON_LEFT)) {
            mouseX = GetMouseX();
            double percentageBar = (mouseX-700.0)/400.0;
            if (percentageBar > 1.0) percentageBar = 1.0;
            if (percentageBar < 0.0) percentageBar = 0.0;
            if (ClickedAngleOfAttack) wind_angle = (180.0 + (percentageBar-0.5)*360.0)*DEG2RAD;
            if (ClickedWindSpeed) wind_speed = percentageBar*WIND_SPEED_MAX;
            if (ClickedCG) pos_CG_TX_B(0) = percentageBar*chord;
            if (ClickedPivot) pos_PV_TX_B(0) = percentageBar*chord; 
        } else if (IsMouseButtonReleased(MOUSE_BUTTON_LEFT)) {
            // reset machine variables
            ClickedAngleOfAttack = false;
            ClickedWindSpeed = false; 
            ClickedCG = false; 
            ClickedPivot = false;
        }

        // draw background
        DrawTextureEx(backgroundTex, Vector2{0,0}, 0 * RAD2DEG, 1, WHITE);
        if ( wind_Pos_I(0) <= -cloudsTex.width*WIND_TEXTURE_SCALE*PX2M/2.0 ) wind_Pos_I(0) += cloudsTex.width*WIND_TEXTURE_SCALE*PX2M/2.0;
        if ( wind_Pos_I(2) <= -cloudsTex.height*WIND_TEXTURE_SCALE*PX2M/2.0 ) wind_Pos_I(2) += cloudsTex.height*WIND_TEXTURE_SCALE*PX2M/2.0;
        if ( wind_Pos_I(0) >= 0.0 ) wind_Pos_I(0) -= cloudsTex.width*WIND_TEXTURE_SCALE*PX2M/2.0;
        if ( wind_Pos_I(2) >= 0.0 ) wind_Pos_I(2) -= cloudsTex.height*WIND_TEXTURE_SCALE*PX2M/2.0;
        DrawTextureEx(cloudsTex, Vector2{(float)(wind_Pos_I(0)*M2PX),(float)(wind_Pos_I(2)*M2PX)}, 0 * RAD2DEG, WIND_TEXTURE_SCALE, WHITE);

        /* get time since last frame display (e.g., display sampling time) */
        double dt{GetFrameTime()};

        state_type x = { airfoil_Pos_Tx_I_I(0), airfoil_Pos_Tx_I_I(1), airfoil_Pos_Tx_I_I(2), 
                         airfoil_Vel_Tx_I_I(0), airfoil_Vel_Tx_I_I(1), airfoil_Vel_Tx_I_I(2),
                         pitch, pitch_rate  };
        boost::numeric::odeint::integrate( airfoil_dynamics , x , 0.0 , dt , dt/3.0 , update_airfoil );
        wind_Pos_I += wind_Vel_I*dt;

        /* draw aileron new configuration */
        // attention! this function takes angles in DEG! (and the angles pitch down)
        Vector2 airfoil_Pos_Tx_I_I_vec2{(float)airfoil_Pos_Tx_I_I(0), (float)airfoil_Pos_Tx_I_I(2)};
        DrawTextureEx(airfoilTex, Vector2Scale(airfoil_Pos_Tx_I_I_vec2, M2PX), -pitch * RAD2DEG, 1.0, WHITE);

        /* update notable points locations */
        pos_PV_CG_B = pos_PV_TX_B - pos_CG_TX_B;
        pos_AC_CG_B = pos_AC_TX_B - pos_CG_TX_B;

        // compute CG position and draw it
        // rotation matrix (window to airfoil) recomputation
        Eigen::Matrix3d Dbi;
        Dbi << cos(pitch), 0, -sin(pitch),
               0         , 1,     0      ,
               sin(pitch), 0,  cos(pitch);
        Eigen::MatrixXd pos_CG_I_I(3,1);
        pos_CG_I_I = airfoil_Pos_Tx_I_I + Dbi.transpose()*pos_CG_TX_B;
        Vector2 pos_CG_I_I_vec2{(float)(pos_CG_I_I(0)*M2PX-cgTex.width/2.0*0.01), (float)(pos_CG_I_I(2)*M2PX-cgTex.height/2.0*0.01)};
        DrawTextureEx(cgTex, pos_CG_I_I_vec2, 0 * RAD2DEG, 0.01, YELLOW);

        // compute PV position and draw it
        Eigen::MatrixXd pos_PV_I_I(3,1);
        pos_PV_I_I = airfoil_Pos_Tx_I_I + Dbi.transpose()*pos_PV_TX_B;
        const double scale = 0.025;
        Vector2 pos_PV_I_I_vec2{(float)(pos_PV_I_I(0)*M2PX-pivotTex.width/2.0*scale), (float)(pos_PV_I_I(2)*M2PX-pivotTex.height/2.0*scale)};
        DrawTextureEx(pivotTex, pos_PV_I_I_vec2, 0 * RAD2DEG, scale, WHITE); 

        // compute AC position and draw it
        Eigen::MatrixXd pos_AC_I_I(3,1);
        pos_AC_I_I = airfoil_Pos_Tx_I_I + Dbi.transpose()*pos_AC_TX_B;
        DrawCircle(M2PX*pos_AC_I_I(0),M2PX*pos_AC_I_I(2), 5.0, GREEN);

        /* update wind angle user inputs */
        data_display_for_inputs(700, 80 + 0*VERTICAL_DISPLAY_OFFSETS, "Wind Incidence (deg)"             , wind_angle*RAD2DEG, wind_angle/2.0/PI);
        data_display_for_inputs(700, 80 + 1*VERTICAL_DISPLAY_OFFSETS, "Wind speed (m/s)"                 , wind_speed, wind_speed/WIND_SPEED_MAX);
        data_display_for_inputs(700, 80 + 2*VERTICAL_DISPLAY_OFFSETS, "Center of Mass location (\%chord)", pos_CG_TX_B(0)/chord*100.0, pos_CG_TX_B(0)/chord);
        data_display_for_inputs(700, 80 + 3*VERTICAL_DISPLAY_OFFSETS, "Pivot location (\%chord)"         , pos_PV_TX_B(0)/chord*100.0, pos_PV_TX_B(0)/chord);

        /* display FPS for debugging and sanity check reasons */
        DrawFPS(1200, 2);

        EndDrawing();

    }

    UnloadTexture(airfoilTex);
    UnloadTexture(cgTex);
    UnloadTexture(pivotTex);

    CloseWindow();
    return 0;
}

double Cd(double alpha) {return 2.0*3.14159*sin(alpha)*sin(alpha);}
double Cy(double alpha) {return 0.0;}
double Cl(double alpha) {return 3.14159*sin(2.0*alpha);}
double Kl(double alpha) {return 0.0;}
double Km(double alpha) {return -0*0.5;}
double Kn(double alpha) {return 0.0;}

void airfoil_dynamics( const state_type &x , state_type &dxdt , double t )
{

        double pitch = x[6];
        double pitch_rate = x[7];

        /* re-compute useful state-dependent quantities */
        // rotation matrix (window to airfoil) recomputation
        Eigen::Matrix3d Dbi;
        Dbi << cos(pitch), 0, -sin(pitch),
               0         , 1,     0      ,
               sin(pitch), 0,  cos(pitch);
        // angle of attack computation
        Eigen::Vector3d v_rel; 
        wind_Vel_I << wind_speed*cos(wind_angle), 0.0, -wind_speed*sin(wind_angle); 
        v_rel = Dbi*(-wind_Vel_I);
        double alpha = (v_rel.norm()>0.001) ? atan2(v_rel(2),v_rel(0)) : 0.0;
        // rotation matrix (wind to body)
        Eigen::Matrix3d Dbw;
        Dbw << cos(alpha), 0, -sin(alpha),
                0        , 1,     0      ,
               sin(alpha), 0,  cos(alpha);
        // aerodynamics force computation
        Eigen::Vector3d aero_force_b;
        aero_force_b << -Cd(alpha),-Cy(alpha),-Cl(alpha);
        aero_force_b = 0.5*rho*Sref*v_rel.norm()*v_rel.norm()*Dbw*aero_force_b;
        // aerodynamics moments computation
        Eigen::Vector3d aero_moment_b;
        aero_moment_b << wingspan*Kl(alpha),chord*Km(alpha),wingspan*Kn(alpha);
        aero_moment_b = 0.5*rho*Sref*v_rel.norm()*v_rel.norm()*aero_moment_b;

        /* 9x9 generalized inertia matrix M in R(9,9) for airfoil dynamics */
        Eigen::MatrixXd M(9,9);
        Eigen::MatrixXd genForce(9,1);

        /* compute generalized inertia matrix of the dynamic system */
        // we consider q_dotdot = ( a_Tx_I_I, w_dot, F_i ) in R(9)
        // we start by the F=ma (forces) equation in inertial coordinates
        M.block(0, 0, 3, 3) << -1,  0,  0,
                                0, -1,  0,
                                0,  0, -1;
        Eigen::Vector3d pos_CG_TX_I;
        pos_CG_TX_I = Dbi.transpose()*pos_CG_TX_B;
        M.block(0, 3, 3, 3) <<  0,              -pos_CG_TX_I(2),  pos_CG_TX_I(1),
                                pos_CG_TX_I(2),  0,              -pos_CG_TX_I(0),
                               -pos_CG_TX_I(1),  pos_CG_TX_I(0),  0;
        M.block(0, 6, 3, 3) <<  1,  0,  0,
                                0,  1,  0,
                                0,  0,  1;
        M.block(0, 6, 3, 3) = M.block(0, 6, 3, 3) * (1.0/mass);
        // we continue by the M=I*alpha (moments) equation in body coordinates
        M.block(3, 0, 3, 3).setZero();
        M.block(3, 3, 3, 3) = -Jb;
        M.block(3, 6, 3, 3) <<  0,              -pos_PV_CG_B(2),  pos_PV_CG_B(1),
                                pos_PV_CG_B(2),  0,              -pos_PV_CG_B(0),
                               -pos_PV_CG_B(1),  pos_PV_CG_B(0),  0;
        M.block(3, 6, 3, 3) = M.block(3, 6, 3, 3)*Dbi;
        // we continue by the linear constraint equation in body coordinates
        M.block(6, 0, 3, 3) = Dbi;
        M.block(6, 3, 3, 3) << 0,              -pos_PV_TX_B(2),  pos_PV_TX_B(1),
                               pos_PV_TX_B(2),  0,              -pos_PV_TX_B(0),
                              -pos_PV_TX_B(1),  pos_PV_TX_B(0),  0;
        M.block(6, 3, 3, 3) = -M.block(6, 3, 3, 3);
        M.block(6, 6, 3, 3).setZero();

        /* compute generalized forces of the dynamic system */
        // we consider q_dotdot = ( a_Tx_I_I, w_dot, F_i ) in R(9)
        // we start by the F=ma (forces) equation  in inertial coordinates
        genForce.block(0, 0, 3, 1) = -(1.0/mass)*Dbi.transpose()*aero_force_b;
        Eigen::Vector3d g_I(0.0, 0.0, 9.81);
        genForce.block(0, 0, 3, 1) += -g_I;
        Eigen::Vector3d ang_vel_B_I_B;
        Eigen::Vector3d ang_vel_B_I_I;
        ang_vel_B_I_B << 0.0, pitch_rate, 0.0;
        ang_vel_B_I_I = Dbi.transpose()*ang_vel_B_I_B;
        genForce.block(0, 0, 3, 1) += ang_vel_B_I_I.cross(ang_vel_B_I_I.cross( Dbi.transpose()*pos_CG_TX_B) );
        // we continue by the M=I*alpha (moments) equation in body coordinates
        genForce.block(3, 0, 3, 1).setZero();
        Eigen::Vector3d pivot_torque_b;
        pivot_torque_b = -0.1*ang_vel_B_I_B;
        genForce.block(3, 0, 3, 1) += -pivot_torque_b;
        genForce.block(3, 0, 3, 1) += -pos_AC_CG_B.cross(aero_force_b);
        genForce.block(3, 0, 3, 1) += -aero_moment_b;
        // we continue by the linear constraint equation in body coordinates
        genForce.block(6, 0, 3, 1).setZero();
        genForce.block(6, 0, 3, 1) += -ang_vel_B_I_B.cross( ang_vel_B_I_B.cross( pos_PV_TX_B ) );

        /* solve for configuration acceleration M qdotdot = genF */
        Eigen::MatrixXd q_ddot(9,1);
        q_ddot = M.colPivHouseholderQr().solve(genForce);

        /* end update phase with updating state values with new buffered values */
        dxdt[0] = x[3];
        dxdt[1] = x[4];
        dxdt[2] = x[5];
        dxdt[3] = q_ddot(0);
        dxdt[4] = q_ddot(1);
        dxdt[5] = q_ddot(2);
        dxdt[6] = x[7];
        dxdt[7] = q_ddot(4);

}

void update_airfoil( const state_type &x , const double t )
{
    airfoil_Pos_Tx_I_I << x[0], x[1], x[2];
    airfoil_Vel_Tx_I_I << x[3], x[4], x[5];
    pitch = x[6];
    pitch_rate = x[7];
}

void data_display_for_inputs(int posX, int posY, const char* display_text, double display_value, double bar_percentage)
{
    // display user chosen value
    DrawText(display_text, posX, posY, 15, WHITE);
    DrawRectangle(posX, posY+20, 400, 20, WHITE);
    DrawRectangle(posX+2, posY+21, bar_percentage*396.0, 18, GREEN);
    char alpha_display[25];
    sprintf(alpha_display, "%f", display_value);
    DrawText(alpha_display, posX, posY+25, 15, BLACK);
}
